package com.perfectotoi.CommonStep;

import java.util.List;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public interface HomePageCommon {

	public QAFWebElement getStoryText();
	
	public QAFWebElement getMenuIcon();
	public QAFWebElement getBackButton();
	public QAFWebElement getSaved_stories_icon();
	public QAFWebElement getLabelSportsSportPage();
}
